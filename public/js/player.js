function Player(context){
    this.context = context;
    this.sounds = {};
    this.gainNode = context.createGain();

    self = this;

    this.load = function load(url) {
      var request = new XMLHttpRequest();

      request.open('GET', url, true);
      request.responseType = 'arraybuffer';
      request.onload = function(){
        context.decodeAudioData( request.response,
                                 function(buffer){ self.sounds[url] = buffer; },
                                 function(error){ console.log(error) } );
      }

      request.send();
    }

  this.play = function(url, time) {
    var source = context.createBufferSource();
    source.buffer = this.sounds[url];
    source.connect(this.gainNode);
    this.gainNode.connect(context.destination);

    source.start(time);
  }

  this.playSexSequence = function(){
    time = this.context.currentTime;
    this.play('sounds/sex01.mp3', time + 0);
    this.play('sounds/sex01.mp3', time + 5);
  }

  this.playForestSequence = function(){
    time = this.context.currentTime;
    this.play('sounds/forest.mp3', time);
    this.play('sounds/forest.mp3', time + 8);
  }

  this.playCameraShoot = function(){
    time = this.context.currentTime;
    this.play('sounds/camera-shutter-click-01.mp3', time + 2);
  }
}
